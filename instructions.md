# Alphabet Soup Solver

This Python script finds words in a grid for a word search puzzle.

## Structure

- `alphabet.py`: The main script.
- `input.txt`: The default puzzle input.
- `/tests`: Contains `test_alphabet.py` and test input files.
- `requirements.txt`: Lists dependencies.

## Setup

1. Clone the repo and navigate into it.
2. (Optional) Create a virtual environment: `python -m venv .venv` and activate it.
3. Install dependencies: `pip install -r requirements.txt`.

## Usage

Run the solver with `python alphabet.py`. To use a custom input file, specify the file path.

## Testing

Execute tests with `python -m unittest tests/test_alphabet.py`. 

For coverage, use `pytest --cov=.` after installing `pytest` and `pytest-cov`.

For any contributions, please ensure tests pass and coverage is adequate.

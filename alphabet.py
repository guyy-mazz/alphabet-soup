def read_input_file(file_path):
    """
    Read the input file and return the grid and list of words.
    """
    with open(file_path, 'r') as file:
        grid_size = file.readline().strip()
        rows, cols = map(int, grid_size.split('x'))
        grid = [file.readline().split() for _ in range(rows)]
        words = [line.strip() for line in file]
    return grid, words


def find_word(grid, word):
    """
    Search for the word in the grid in all directions and return its position.
    """
    for row in range(len(grid)):
        for col in range(len(grid[row])):
            result = search_word(grid, word, row, col)
            if result:
                start_row, start_col, end_row, end_col = result
                return start_row, start_col, end_row, end_col
    return None


def search_word(grid, word, start_row, start_col):
    """
    Search for the word starting from a specific position in the grid.
    """
    directions = [
        (0, 1),  # Right
        (1, 0),  # Down
        (1, 1),  # Diagonal right down
        (0, -1),  # Left
        (-1, 0),  # Up
        (-1, -1),  # Diagonal left up
        (1, -1),  # Diagonal left down
        (-1, 1)  # Diagonal right up
    ]
    if not word:  # Check if the word is empty
        return None
    for dr, dc in directions:
        end_row, end_col = start_row, start_col
        found = True
        for i in range(len(word)):
            r, c = start_row + dr * i, start_col + dc * i
            if r < 0 or r >= len(grid) or c < 0 or c >= len(grid[0]) or grid[r][c] != word[i]:
                found = False
                break
            end_row, end_col = r, c
        if found:
            return start_row, start_col, end_row, end_col
    return None


def main(file_path):
    """
    Main function to process the input file and find the words in the grid.
    """
    grid, words = read_input_file(file_path)
    for word in words:
        position = find_word(grid, word)
        if position:
            print(
                f"{word} {position[0]}:{position[1]} {position[2]}:{position[3]}")


def run_main():
    file_path = "input.txt"
    main(file_path)


if __name__ == "__main__":
    run_main()

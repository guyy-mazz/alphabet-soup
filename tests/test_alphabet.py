import unittest
from alphabet import read_input_file, find_word, search_word, run_main, main
from unittest.mock import mock_open, patch, MagicMock
import io


class TestWordSearch(unittest.TestCase):
    # Assume this test file structure is correct
    def test_read_input_file(self):
        grid, words = read_input_file('input.txt')
        self.assertEqual(len(grid), 5)
        self.assertEqual(len(words), 3)
        # More assertions can be added here...

    def test_find_word_horizontal(self):
        grid = [['A', 'B', 'C', 'D'],
                ['E', 'F', 'G', 'H'],
                ['I', 'J', 'K', 'L'],
                ['M', 'N', 'O', 'P']]
        word = 'ABCD'
        self.assertEqual(find_word(grid, word), (0, 0, 0, 3))

    def test_find_word_vertical(self):
        grid = [['A', 'B', 'C', 'D'],
                ['E', 'F', 'G', 'H'],
                ['I', 'J', 'K', 'L'],
                ['M', 'N', 'O', 'P']]
        word = 'AEIM'
        self.assertEqual(find_word(grid, word), (0, 0, 3, 0))

    def test_find_word_diagonal(self):
        grid = [['A', 'B', 'C', 'D'],
                ['E', 'F', 'G', 'H'],
                ['I', 'J', 'K', 'L'],
                ['M', 'N', 'O', 'P']]
        word = 'AFKP'
        self.assertEqual(find_word(grid, word), (0, 0, 3, 3))

    def test_find_word_backward(self):
        grid = [['A', 'B', 'C', 'D'],
                ['E', 'F', 'G', 'H'],
                ['I', 'J', 'K', 'L'],
                ['M', 'N', 'O', 'P']]
        word = 'DCBA'
        self.assertEqual(find_word(grid, word), (0, 3, 0, 0))

    def test_find_word_not_found(self):
        grid = [['A', 'B', 'C', 'D'],
                ['E', 'F', 'G', 'H'],
                ['I', 'J', 'K', 'L'],
                ['M', 'N', 'O', 'P']]
        word = 'XXXX'
        self.assertIsNone(find_word(grid, word))

    def test_search_word_invalid_start(self):
        grid = [['A', 'B', 'C', 'D'],
                ['E', 'F', 'G', 'H'],
                ['I', 'J', 'K', 'L'],
                ['M', 'N', 'O', 'P']]
        word = 'ABCD'
        self.assertIsNone(search_word(grid, word, -1, -1)
                          )  # Invalid start indices

    def test_empty_grid(self):
        # Test with an empty grid
        grid = []
        word = 'ANY'
        self.assertIsNone(find_word(grid, word))

    def test_read_input_file_with_invalid_path(self):
        # Test with an invalid file path
        with self.assertRaises(FileNotFoundError):
            read_input_file('non_existent_file.txt')

    def test_read_input_file_with_invalid_format(self):
        # Test with an invalid format in the input file
        # You'll have to create a 'bad_input.txt' with incorrect format for this test
        with self.assertRaises(ValueError):
            read_input_file('bad_input.txt')

    def test_find_word_with_empty_word(self):
        # Test searching for an empty string as a word
        grid = [['A', 'B', 'C'], ['D', 'E', 'F']]
        word = ''
        print("="*100)
        print(find_word(grid, word))
        print("="*100)
        self.assertIsNone(find_word(grid, word))


class TestMainFunction(unittest.TestCase):
    def test_main_with_valid_input(self):
        mock_file_content = "2x2\nA B\nC D\nAB\nCD"
        expected_output = "AB 0:0 0:1\nCD 1:0 1:1\n"
        with patch('builtins.open', mock_open(read_data=mock_file_content)), \
                patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
            main('input.txt')
            self.assertEqual(mock_stdout.getvalue(), expected_output)

    def test_main_with_no_words_found(self):
        mock_file_content = "2x2\nA B\nC D\nXY\nZQ"
        expected_output = ""
        with patch('builtins.open', mock_open(read_data=mock_file_content)), \
                patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
            main('input.txt')
            self.assertEqual(mock_stdout.getvalue(), expected_output)

    def test_main_with_invalid_path(self):
        with patch('builtins.open', MagicMock(side_effect=FileNotFoundError)), \
                patch('sys.stderr', new_callable=io.StringIO) as mock_stderr:
            with self.assertRaises(FileNotFoundError):
                main('invalid_path.txt')
            self.assertIn('', mock_stderr.getvalue())

    def test_main_with_empty_grid(self):
        mock_file_content = "0x0\n\nAB\nCD"
        expected_output = ""
        with patch('builtins.open', mock_open(read_data=mock_file_content)), \
                patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
            main('input.txt')
            self.assertEqual(mock_stdout.getvalue(), expected_output)

    def test_main_with_incorrect_format(self):
        mock_file_content = "2x\nA B\nC D\nAB\nCD"  # Invalid grid size format
        with patch('builtins.open', mock_open(read_data=mock_file_content)), \
                patch('sys.stderr', new_callable=io.StringIO) as mock_stderr:
            with self.assertRaises(ValueError):
                main('input.txt')
            self.assertIn('',
                          mock_stderr.getvalue())

    @patch('alphabet.main')
    def test_run_main(self, mock_main):
        # Mock the `open` function in the `main` module
        mock_file_content = "2x2\nA B\nC D\nAB\nCD"
        with patch('builtins.open', mock_open(read_data=mock_file_content)):
            # Now call the function that triggers `main`
            run_main()
            # And check if it was called with the expected argument
            mock_main.assert_called_once_with("input.txt")


if __name__ == '__main__':
    unittest.main()
